import React, { useState } from "react";
import injectSheet from "react-jss";
import cx from "classnames";
import Button from "./Button";

const styles = {
  rainbowWrapper: {
    flex: 1
  },
  wrapper: {
    display: "flex",
    flexDirection: "column"
  },
  buttonBlock: {
    margin: "1.2em",
    fontSize: "1.5em"
  },
  image: {
    flex: 1
  }
};

function Rainbow({ classes }) {
  const [isShown, setRaindow] = useState(false);
  return (
    <div className={cx(classes.rainbowWrapper, classes.wrapper)}>
      <div className={cx(classes.wrapper, classes.buttonBlock)}>
        <div>Show rainbow?</div>
        <div>
          <Button
            label="-"
            disabled={!isShown}
            clickHandler={() => setRaindow(false)}
          />
          <Button
            label="+"
            disabled={isShown}
            clickHandler={() => setRaindow(true)}
          />
        </div>
      </div>
      {isShown && (
        <img
          className={classes.image}
          src="http://xn--h1adaolkc5e.ot7.ru/admin/uploads/1/0/7/%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%BA%D0%B8-%D1%81-%D1%80%D0%B0%D0%B4%D1%83%D0%B3%D0%BE%D0%B9-gif-%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%BA%D0%B8-%D1%81-%D1%80%D0%B0%D0%B4%D1%83%D0%B3%D0%BE%D0%B9-%D1%81%D0%BA%D0%B0%D1%87%D0%B0%D1%82%D1%8C-%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%BE-13630.gif"
          alt="raindow"
        />
      )}
    </div>
  );
}

export default injectSheet(styles)(Rainbow);
