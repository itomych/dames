import React from "react";
import Unicorns from "./Unicorns";
import Rainbow from "./Raindow";
import { unicorns } from "./data";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Unicorns unicorns={unicorns} />
      <Rainbow />
    </div>
  );
}

export default App;
