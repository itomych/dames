// @flow
import React from "react";
import injectSheet from "react-jss";

type PropsT = {
  disabled: boolean,
  clickHandler: Function,
  label: string,
  classes: Object
};

const styles = {
  button: {
    width: "3em",
    height: "2em",
    margin: "0.5em",
    borderRadius: "1em",
    backgroundColor: "pink",
    fontSize: "1em"
  }
};

const Button = (props: PropsT) => (
  <button
    disabled={props.disabled}
    onClick={props.clickHandler}
    className={props.classes.button}
  >
    {props.label}
  </button>
);

export default injectSheet(styles)(Button);
