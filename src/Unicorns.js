// @flow
import React from "react";
import injectSheet from "react-jss";
import Button from "./Button";

type UnicornT = {
  id: string,
  url: string
};

type PropsT = {
  unicorns: UnicornT[]
};

type StateT = {
  length: number
};

const styles = {
  unicornsWrapper: {
    display: "flex",
    justifyContent: "space-around",
    minHeight: "384px"
  },
  image: {
    maxWidth: "20%"
  },
  buttonsWrapper: {
    margin: "1em",
    fontSize: "1.5em"
  }
};

class Unicorns extends React.Component<PropsT, StateT> {
  constructor(props: PropsT) {
    super(props);
    this.state = {
      length: 1 // props.unicorns.length
    };
  }

  renderUnicorns() {
    const shownLength = this.props.unicorns.slice(0, this.state.length);
    return shownLength.map((unicorn: UnicornT) => (
      <img
        key={unicorn.id}
        src={unicorn.url}
        className={this.props.classes.image}
      />
    ));
  }

  render() {
    return (
      <div>
        <div className={this.props.classes.unicornsWrapper}>
          {this.renderUnicorns()}
        </div>
        <div className={this.props.classes.buttonsWrapper}>
          <div>How many unicorns do you want to see?</div>
          {this.props.unicorns.map((unicorn: UnicornT) => (
            <Button
              key={`button-${unicorn.id}`}
              disabled={this.state.length === +unicorn.id}
              label={unicorn.id}
              clickHandler={() => this.setState({ length: +unicorn.id })}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default injectSheet(styles)(Unicorns);

//     {
//       "id": "6",
//       "url": "https://media0.giphy.com/media/26xBMTrIhFT1YYe7m/source.gif"
//     }
